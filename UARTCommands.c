#include "Commands.h"
#include "UART.h"
#include "Error.h"
#include "Actions.h"
#include "Utils.h"

static int index = 0;
static unsigned char command[COMMAND_BUFF_SIZE];


int can_execute_command(void)
{
    return index && (command[index - 1] == COMMAND_END);
}

int command_try_consume_byte()
{
    unsigned char c;
    
    int result = UART_try_read(&c);
    if (result) return result;
   
    if (c == COMMAND_RESET) {
        index = 0;
        return 0;
    }
    
    if (index > COMMAND_BUFF_SIZE) {
        index = 0;
        return report_error(ERROR_COMMAND_BUFF_OVERFLOW);
    }

    command[index++] = c;

    return 0;
}

int execute_command(void)
{    
    //  We don't need the command size anymore. Each 
    // action can figure out the amount of data it needs.
    index = 0;
 
    unsigned char* password = &command[6];
    User* user = match_user_pass(password);
    
    int group  = extract_3_digit_num(command);
    int action = extract_3_digit_num(command + 3);
    void* data = (void*)&command[6 + USER_PASS_SIZE];
    
    return perform_action(user, group, action, data);
}

int get_num(int i) { return command[i] - '0'; }