#pragma once

#include "Users.h"
#include <stdint.h>

#define ACTION_GROUP_COUNT 8  // Since the permissions is represented with an unit8_t

typedef int (*Action)(void* data);

typedef struct
{
    uint8_t index;
    Action* actions;
    
} ActionGroup;

ActionGroup action_groups[ACTION_GROUP_COUNT];

int perform_action(User* const user, uint8_t group, uint8_t action, void* data);
int add_action_group(ActionGroup* group);
Action* request_actions_array(uint8_t size);

//  No need for a delete action group operation since this is not done
// dynamically at runtime, and the action groups don't change through
// out the program execution.