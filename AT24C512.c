#pragma once

#include "I2C.h"
#include "CONFIG.h"
#include "AT24C512.h"
#include "Error.h"

int EEPROM_set_counter(unsigned int address)
{
    I2C_wait();
    I2C_start();
    I2C_wait();
    while (!SSPIF);
    SSPIF = 0;
    
    // The last iteration would execute the write before checking for
    // exceeding the maximum number of polls, thus, performing a write
    // trial after each repeated start
    int polls_left = MAX_POLLING_TRIALS;
    while (I2C_write(EEPROM_Address_W) && polls_left--)
        I2C_repeated_start();
    
    if (!polls_left)
        return report_error(ERROR_EXHAUSTED_ALL_TRIALS);
    
    unsigned char high = address >> 8;
    unsigned char low = address & 0x00FF;
    
    if (I2C_write(high))
        return report_error(ERROR_WRITE_ADDR_HIGH);
    
    if (I2C_write(low))
        return report_error(ERROR_WRITE_ADDR_LOW);

    return 0;
}

int EEPROM_read_page(unsigned int address, unsigned char* data, unsigned char len)
{
    // LEN MUST BE >= 1
    
    // Dummy write to set the word address counter.
    int ret = EEPROM_set_counter(address);
    if (ret) return ret;
    
    I2C_repeated_start();
    while (!SSPIF) {
        
    };
    SSPIF = 0;
    if (I2C_write(EEPROM_Address_R))
        return report_error(ERROR_SEND_READ_ADDR);
    
    for(unsigned char i = 0; i < len - 1; i++) {
        data[i] = I2C_read();
        I2C_ACK();
    }
    //  Should send a NACK after the last byte, not an ACK.
    data[len - 1] = I2C_read();
    I2C_NACK();
    
    I2C_stop();
    
    return 0;
}

int EEPROM_write_page(unsigned int address, unsigned char* data, unsigned char len)
{
    // Write a maximum of 128 bytes
    
    int ret = EEPROM_set_counter(address);
    if (ret) return ret;
        
    for (unsigned char i = 0; i < len; i++)
        if (I2C_write(data[i]))
            return report_error(ERROR_WRITE_ITH_BYTE(i));
    
    I2C_stop();
    
    return 0;
}