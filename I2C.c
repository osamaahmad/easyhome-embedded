#include "CONFIG.h"
#include "I2C.h"

void I2C_init_master(const unsigned long baud)
{
    TRISB0 = 1;
    TRISB1 = 1;
    SSPCON1 = 0x28;
    SSPADD = (_XTAL_FREQ / (4 * baud)) - 1;
}

void I2C_wait(void)
{
    while ((SSPSTAT & 0x04) || (SSPCON2 & 0x1F));
}

void I2C_start(void)
{
    I2C_wait();
    SEN = 1;
}

void I2C_repeated_start(void)
{
    I2C_wait();
    RSEN = 1;
}

void I2C_stop(void)
{
    I2C_wait();
    PEN = 1;
}

void I2C_ACK(void)
{
    ACKDT = 0;
    I2C_wait();
    ACKEN = 1;
}

void I2C_NACK(void)
{
    ACKDT = 1;
    I2C_wait();
    ACKEN = 1;
}

unsigned char I2C_write(unsigned char data)
{
    I2C_wait();
    SSPBUF = data;
    while(!SSPIF);
    SSPIF = 0;
    return ACKSTAT;
}

unsigned char I2C_read(void)
{
    I2C_wait();
    RCEN = 1;  // Not RSEN
    while(!SSPIF);
    SSPIF = 0;
    I2C_wait();
    return SSPBUF;
}
