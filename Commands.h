#pragma once

#define COMMAND_BUFF_SIZE 256
#define COMMAND_END '!'
#define COMMAND_RESET '^'

/*
 * Expected command format:
 *      XXX(3) YYY(3) PASSWORD(USER_PASS_SIZE) DATA(?) COMMAND_END
 * where:
 *      XXX = the desired action group
 *      YYY = the desired option within the action group
 *      DATA = the data that will be passed to the action
 *      COMMAND_END = a character indicating the end of the command
 *  There are no spaces in between the parts, and are
 * only added here for clarity. More specifically:
 *      Let n = the sent command size
 *      command[0..3] = action gropu
 *      command[3..6] = operation
 *      command[6..6 + USER_PASS_SIZE] = password
 *      command[6 + USER_PASS_SIZE..?] = data
 *      command[n - 1] = COMMAND_END
 *  Note that since each byte can represent from 0 up to 
 * 256, we can represent action groups and operations
 * using a single byte each, but it's more convinent
 * this way for debugging. 
 *  The data size can be arbitrary. actions know exactly how
 * much data they need.
 *  You can send COMMAND_RESET to reset the command index to 0
 */

int can_execute_command(void);
int command_try_consume_byte();
int execute_command(void);
