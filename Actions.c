#include "Actions.h"
#include "Error.h"
#include <stdlib.h>

#define MAX_ACTIONS_NUM 20

static uint8_t index = 0;
static Action storage[MAX_ACTIONS_NUM];

int perform_action(User* const user, uint8_t group, uint8_t action, void* data)
{
    if (user && (user->permissions & (1 << group)))
        return action_groups[group].actions[action](data);    
    
    return report_error(ERROR_NO_PERMISSION);
}

int add_action_group(ActionGroup* group)
{
    action_groups[group->index] = *group;
    return 0;
}

Action* request_actions_array(uint8_t size) 
{
    if (index + size >= MAX_ACTIONS_NUM) {
        report_error(ERR0R_MAX_ACTINOS_EXCEEDED);
        return NULL;
    }
    Action* result = storage + index;
    index += size;
    return result;
}