#include "EEPROM.h"
#include "AT24C512.h"

int read(uint16_t address, uint8_t* data, uint8_t len)
{
    return EEPROM_read_page(address, data, len);
}

int write(uint16_t address, uint8_t* data, uint8_t len)
{
    return EEPROM_write_page(address, data, len);
}
