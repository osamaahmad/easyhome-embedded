#include "Utils.h"
#include "CONFIG.h"

void blink_binary(unsigned int value)
{
    while (value)
    {   
        LATA1 = 0;
        __delay_ms(50);
        LATA1 = 1;
        
        LATA0 = value & 1;
        value >>= 1;
      
        __delay_ms(500);
    }
    LATA0 = 0;
    LATA1 = 0;
}

int extract_3_digit_num(const unsigned char* buff) 
{
    return (buff[0] - '0') * 100 + (buff[1] - '0') * 10 + (buff[2] - '0');
}
