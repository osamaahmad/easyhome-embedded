#include "Users.h"
#include "Utils.h"

// -- User wrappers ------------------------------------------------

int __add_user(void* data)
{
    User user = *((User*)data);
    uint8_t* p_start = (uint8_t*)data + offsetof(User, permissions);
    user.permissions = extract_3_digit_num(p_start);
    return add_user(&user);
}

int __remvoe_user(void* data)
{
    return remove_user_by_name(data);
}

int __set_user_name(void* data)
{
    unsigned char* old_name = (unsigned char*)data;
    unsigned char* new_name = old_name + USER_NAME_SIZE;
    return set_user_name_by_name(old_name, new_name);
}

int __set_user_password(void* data)
{
    unsigned char* name = (unsigned char*)data;
    unsigned char* password = name + USER_NAME_SIZE;
    return set_user_password_by_name(name, password);
}

int __set_user_permissions(void* data)
{
    unsigned char* name = (unsigned char*)data;
    uint8_t permissions = extract_3_digit_num(name + USER_NAME_SIZE);
    return set_user_permissions_by_name(name, permissions);
}

int __send_user(void* data)
{
    return send_user_by_name((unsigned char*)data);
}