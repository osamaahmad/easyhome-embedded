#pragma once

#include <stdint.h>
#include <stddef.h>

#define USER_NAME_SIZE 5
#define USER_PASS_SIZE 5
#define USERS_ACTION_INDEX 0
#define USERS_CACHE_SIZE 10

//  The rest of the struct starting from
// the index is not stored in the EEPROM
#define USER_SIZE offsetof(User, index)

#define MASTER_INDEX 1
#define INIT_MASTER_NAME "admin"
#define INIT_MASTER_PASS "admin"
#define INIT_MASTER_PERM ((uint8_t)-1)

typedef struct
{
    // Contains only the size at the moment
    // This header reserves the size of a 
    //  single user struct to ease the math
    uint8_t n;
} UsersInfoHeader;

typedef struct 
{
    unsigned char name[USER_NAME_SIZE];
    // The password hashing is done at the client side
    unsigned char password[USER_PASS_SIZE];
    // Each bit represents a permission to a certain action
    uint8_t  permissions;
    
    // The members starting from here are not stored -----
    
    // The index in the EEPROM
    uint8_t index;
} User;

/*
 *  Indices at the EEPROM starts from 1. Index 0 is
 * reserved for the information header, which contains
 * information about the users, such as their count.
 *  There is always a master user, always at index 1
 * with all permissions. The initial name and password
 * of the master will be hard-coded, and can be reset
 * physically by a button in case of an issue.
 *  Only users with a permission to edit users info can
 * do it.
 *  There is a cache of the most recent users to avoid 
 * searching through all users each time an operation is
 * done. Only on instance of a user is loaded into the cache
 * at any time. Changes to users reflect both in the cache and
 * in the EEPROM instantly. The EEPROM is changed first, so that
 * in case of an error writing to the EEPROM, stop the operation
 * before changing the content in the cache, so that the cached
 * user represents a valid user in the EEPROM. Adding a user adds
 * it automatically in the cache, and removing a user deletes it
 * automatically from the cache.
 *  For a factory reset, just reset all, which removes all non-master
 * users (actually, just sets the users count in the info header to 1),
 * and resets the master's name and password.
 *  A user with a permission can request to view users info. This can
 * be used to list all users on the system. 
 */

uint8_t users_count();
int add_user(User* user);
int remove_user(uint8_t index);
User* const get_user(uint8_t index);
User* const match_user_name(unsigned char* name);
User* const match_user_pass(unsigned char* pass);
int load_user_in_cache(uint8_t index);  // Returns its index in the cache in case of no error.
int set_user_name(uint8_t index, const unsigned char* name);
int set_user_password(uint8_t index, const unsigned char* password);
int set_user_permissions(uint8_t index, uint8_t permissions);
int send_user(uint8_t index);
uint8_t reset_master();
uint8_t reset_all();
void update_cached_count(uint8_t n);

int remove_user_by_name(const unsigned char* name);
int set_user_name_by_name(const unsigned char* name, const unsigned char* new_name);
int set_user_password_by_name(const unsigned char* name, const unsigned char* password);
int set_user_permissions_by_name(const unsigned char* name, uint8_t permissions);
int send_user_by_name(const unsigned char* name);

// Wrapper functions compliant to the action signature
int __add_user(void* data);
int __remvoe_user(void* data);
int __set_user_name(void* data);
int __set_user_password(void* data);
int __set_user_permissions(void* data);
int __send_user(void* data);