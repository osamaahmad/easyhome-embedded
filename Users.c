#include "Users.h"
#include "EEPROM.h"
#include "Error.h"
#include "Utils.h"
#include <string.h>
#include <stddef.h>

//  EEPROM errors are not reported here since it's assumed
// that its own function report in case of an error.

int write_header(UsersInfoHeader* header);
User* get_non_const_user(uint8_t index);
void increment_cache_index();
void invalidate_cache(uint8_t index);
void insert_and_cache_user(User* user, uint8_t index);
void set_users_count(uint8_t n);
int validate_index(uint8_t index);

static uint8_t cache_index = 0; 

static uint8_t cached_n;
static User cache[USERS_CACHE_SIZE];


uint8_t users_count()
{
    return cached_n;
}

int add_user(User* user)
{
    if (user == NULL)
        return report_error(ERROR_INVALID_USER_STRUCT);
    
    uint8_t n = users_count() + 1;
    
    // the indices are 1-based since the header takes the 0th index.
    insert_and_cache_user(user, n);
    set_users_count(n);
    
    return 0;
}

int remove_user(uint8_t index)
{
    int ret = validate_index(index);
    if (ret) return ret;
    
    //  Since the order of the users doesn't matter, just
    // swap with the last user and remove the last user.
    
    uint8_t n = users_count();
    
    User* last = get_user(n);
    write(index * USER_SIZE, (uint8_t)last, USER_SIZE);    
    invalidate_cache(index);
    set_users_count(n - 1);
    
    return 0;
}

User* const get_user(uint8_t index)
{
    return get_non_const_user(index);
}

User* const match_user_name(unsigned char* name)
{
    //  Given that the number of users is not likely to
    // exceed the cache size, putting the users in the
    // cache is more efficient
    int n = users_count();
    for (int i = 1; i <= n; i++) {
        User* const user = get_user(i);
        if (strncmp(user->name, name, USER_NAME_SIZE) == 0)
            return user;
    }
    
    return NULL;
}

User* const match_user_pass(unsigned char* password)
{
    //  Given that the number of users is not likely to
    // exceed the cache size, putting the users in the
    // cache is more efficient
    int n = users_count();
    for (int i = 1; i <= n; i++) {
        User* const user = get_user(i);
        if (strncmp(user->password, password, USER_PASS_SIZE) == 0)
            return user;
    }
    
    return NULL;
}

int load_user_in_cache(uint8_t index)
{
    int ret = validate_index(index);
    if (ret) return ret;
    
    //  It might compare with empty slots, but
    // it doesn't hurt since the indices will
    // be all zeros anyway, and no user with
    // index 0 exists.
    for (int i = 0; i < USERS_CACHE_SIZE; i++)
        if (cache[i].index == index)
            return i;
    
    read(index * USER_SIZE, &cache[cache_index], USER_SIZE);
    cache[cache_index].index = index;
    
    uint8_t result = cache_index;
    increment_cache_index();

    return result;
}

int set_user_name(uint8_t index, const unsigned char* name)
{   
    int ret = validate_index(index);
    if (ret) return ret;
    
    // Updating the value in the cache
    User* user = get_non_const_user(index);
    strncpy(user->name, name, sizeof(user->name));
     
    // Note the copying from user->name instead of the passed string.
    // If strlen(name) < sizeof(user->name), strncpy pads the rest with zeros.
    write(index * USER_SIZE + offsetof(User, name), &user->name, sizeof(user->name));
    
    return 0;
}

int set_user_password(uint8_t index, const unsigned char* password)
{
    int ret = validate_index(index);
    if (ret) return ret;
    
    // Updating the value in the cache
    User* user = get_non_const_user(index);
    strncpy(user->password, password, sizeof(user->password));
    
    // Note the copying from user->password instead of the passed string.
    // If strlen(password) < sizeof(user->password), strncpy pads the rest with zeros.
    write(index * USER_SIZE + offsetof(User, password), user->password, sizeof(user->password));
    
    return 0;
}

int set_user_permissions(uint8_t index, uint8_t permissions)
{
    int ret = validate_index(index);
    if (ret) return ret;
    
    // Updating the value in the cache
    User* user = get_non_const_user(index);
    user->permissions = permissions;
            
    write(index * USER_SIZE + offsetof(User, permissions), &permissions, sizeof(permissions));    
    
    return 0;
}

int send_user(uint8_t index)
{
    // TODO send the user struct to the app
    return 0;
}

uint8_t reset_master()
{
    User master;
    strncpy(master.name, INIT_MASTER_NAME, USER_NAME_SIZE);
    strncpy(master.password, INIT_MASTER_PASS, USER_PASS_SIZE);
    master.permissions = INIT_MASTER_PERM;
    master.index = MASTER_INDEX;
    insert_and_cache_user(&master, MASTER_INDEX);
    return 0;
}

uint8_t reset_all()
{
    reset_master();
    set_users_count(1);
    return 0;
}

int remove_user_by_name(const unsigned char* name)
{
    User* user = match_user_name(name);
    if (user == NULL)
        return report_error(ERROR_USER_NAME_NOT_FOUND);
    
    return remove_user(user->index);
}

int set_user_name_by_name(const unsigned char* name, const unsigned char* new_name)
{
    User* user = match_user_name(name);
    if (user == NULL)
        return report_error(ERROR_USER_NAME_NOT_FOUND);

    return set_user_name(user->index, new_name);
}

int set_user_password_by_name(const unsigned char* name, const unsigned char* password)
{
    User* user = match_user_name(name);
    if (user == NULL)
        return report_error(ERROR_USER_NAME_NOT_FOUND);
    
    return set_user_password(user->index, password);
}

int set_user_permissions_by_name(const unsigned char* name, uint8_t permissions)
{
    User* user = match_user_name(name);
    if (user == NULL)
        return report_error(ERROR_USER_NAME_NOT_FOUND);
    
    return set_user_permissions(user->index, permissions);
}

int send_user_by_name(const unsigned char* name)
{
    User* user = match_user_name(name);
    if (user == NULL)
        return report_error(ERROR_USER_NAME_NOT_FOUND);
    
    return send_user(user);
}

int write_header(UsersInfoHeader* header)
{
    return write(0, (uint8_t*)header, sizeof(*header));
}

void update_cached_count(uint8_t n)
{
    cached_n = n;
}

User* get_non_const_user(uint8_t index)
{
    int result = load_user_in_cache(index);
    if (result < 0) return NULL;
    return &cache[result];
}

void increment_cache_index(void)
{
    cache_index++;
    if (cache_index == USERS_CACHE_SIZE)
        cache_index = 0;
}

void invalidate_cache(uint8_t index)
{
    for (int j = 0; j < USERS_CACHE_SIZE; j++) 
    {
        if (cache[j].index == index) 
        {
            for (int i = 0; i < USER_PASS_SIZE; i++)
                cache[index].password[i] = 0;
            cache[index].index = (uint8_t)-1; // a quick hack
            return;
        }
    }
}

void insert_and_cache_user(User* user, uint8_t index)
{
    write(index * USER_SIZE, (uint8_t*)user, USER_SIZE);
    cache[cache_index] = *user;
    cache[cache_index].index = index;
    increment_cache_index();
}

void set_users_count(uint8_t n)
{
    //  This function is used to insure that the
    // cached count is always updated along with
    // the size in the information header.
    
    UsersInfoHeader header = { n };
    write_header(&header);
    
    update_cached_count(n);
}

int validate_index(uint8_t index)
{
    if (index < 1 || index > users_count()) {
        return report_error(ERROR_INVALID_USER_INDEX);
    }
    return 0;
}