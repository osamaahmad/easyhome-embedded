#pragma once

void I2C_init_master(const unsigned long baud);
void I2C_wait(void);
void I2C_start(void);
void I2C_repeated_start(void);
void I2C_stop(void);
void I2C_ACK(void);
void I2C_NACK(void);
unsigned char I2C_write(unsigned char data);
unsigned char I2C_read(void);