#include "CONFIG.h"
#include "Commands.h"
#include "Users.h"
#include "Actions.h"

int init(void);

void __interrupt() IRQ()
{
    command_try_consume_byte();
}

int try_match(void* name)
{
    if (match_user_name((unsigned char*)name) != NULL) {
        LATA0 = 1;
        __delay_ms(1000);
        LATA0 = 0;
        __delay_ms(1000);
    } else {
        LATA1 = 1;
        __delay_ms(1000);
        LATA1 = 0;
        __delay_ms(1000);
    }
}

void setup_example(void)
{
    reset_all();
    Action* action = request_actions_array(1);
    *action = try_match;
    ActionGroup group = {5, action};
    add_action_group(&group);
}

void main(void) 
{
    init();
    TRISA = 0;
    LATA = 0;

    setup_example();
    
    while (1)
    {
        if (can_execute_command())
            execute_command();
    }
}
