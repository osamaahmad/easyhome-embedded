#pragma once

#include "CONFIG.h"
#include "UART.h"
#include "Error.h"

void UART_init(void)
{    
    // Serial port enable
    SPEN = 1;
    
    // Asynchronous mode
    SYNC = 0;
    
    // High speed baud rate
    BRGH = 1;
    BRG16 = 1;    
    
    // Baud Rate = 9600
    SPBRGH = 0b100;
    SPBRG = 0b11100001;

    // Continuous reception
    CREN = 1;
    
    // Set TX and RX pins as output
    TRISC |= 0b11000000;
}

int UART_try_read(unsigned char* c)
{
    if (RCIF)
    {
        if (OERR) {
            CREN = 0;
            CREN = 1;
            return report_error(ERROR_OVERRUN);
        }
        
        *c = RCREG;
        
        return 0;
    }
    
    //  The error here is not reported
    // since this might be used in polling.
    return ERROR_NO_UART_MESSAGE;
}