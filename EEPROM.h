#pragma once

#include <stdint.h>

int read(uint16_t address, uint8_t* data, uint8_t len);
int write(uint16_t address, uint8_t* data, uint8_t len);
