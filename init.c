#include "CONFIG.h"
#include "Error.h"
#include "I2C.h"
#include "Users.h"
#include "Actions.h"
#include "EEPROM.h"
#include "UART.h"

#define I2C_BAUD_RATE 100000

int init_users(void);
int init_system1(void);
int enable_system1(void* data);

int init(void)
{
    // TODO check for the errors in each init
    I2C_init_master(I2C_BAUD_RATE);
    UART_init();
    init_users();
    init_system1();
    
    // Enable interrupts
    GIE = 1;
    PEIE = 1;
    RCIE = 1;

    // Set analog pins to be digital
    ADCON1 |= 0b00001111;
    
    return 0;
}

int init_users(void)
{
    ActionGroup group;
    group.index = USERS_ACTION_INDEX;
    group.actions = request_actions_array(6);
    group.actions[0] = __add_user;
    group.actions[1] = __remvoe_user;
    group.actions[2] = __set_user_name;
    group.actions[3] = __set_user_password;
    group.actions[4] = __set_user_permissions;
    group.actions[5] = __send_user;
    add_action_group(&group);
    
    UsersInfoHeader header;
    read(0, (uint8_t*)&header, sizeof(header));
    update_cached_count(header.n);
    
    return 0;
}

// An externally-powered system that is enabled by a control signal
int init_system1(void)
{
    ActionGroup group;
    group.index = SYSTEM1_ACTION_INDEX;
    group.actions = request_actions_array(1);
    group.actions[0] = enable_system1;
    add_action_group(&group);
    return 0;
}

int enable_system1(void* data)
{
    SYSTEM1_PIN = 1;
    __delay_ms(200);
    SYSTEM1_PIN = 0;
    return 0;
}