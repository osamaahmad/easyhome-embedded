#pragma once

// Up to 4 EEPROMs are allowed on the same bus
//  Change the bits here        vv for a different EEPROM
#define EEPROM_Address_W 0b10100000
#define EEPROM_Address_R 0b10100001
#define MAX_POLLING_TRIALS 1000

int EEPROM_set_counter(unsigned int address);
int EEPROM_read_page(unsigned int address, unsigned char* data, unsigned char len);
int EEPROM_write_page(unsigned int address, unsigned char* data, unsigned char len);
